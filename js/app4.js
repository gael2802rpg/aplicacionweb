function cargarDatos(){
    
    const http = new XMLHttpRequest;
    const bus = document.getElementById('buscar').value;

    if(bus==''){
        alert('Porfavor ingrese el id que desee buscar');
    }
    else{
        if(bus >= 1 && bus <= 10){
            const url = "https://jsonplaceholder.typicode.com/users/"+bus;
            //Realizar funcion de respuesta 
            http.onreadystatechange = function(){
                //validar respuesta
                if(this.status == 200 && this.readyState==4){
                    let res = document.getElementById('lista');
                    const json = JSON.parse(this.responseText);
                    const datos = json;
                    //ciclo para mostrar los datos en la tabla
                    const address = datos.address;
                    const geo = address.geo; 
                    const company = datos.company;

                    res.innerHTML = '<tr> <td class="columna1">' + datos.id + '</td>'
                    + ' <td class="columna2">' + datos.name + '</td>'
                    + ' <td class="columna3">' + datos.username + '</td>'
                    + ' <td class="columna4">' + datos.email + '</td>'
                    + ' <td class="columna5"> <ul> <li>' + address.street + '</li>'
                    + ' <li> ' + address.suite + '</li>'
                    + ' <li> ' + address.city + '</li>'
                    + ' <li> ' + address.zipcode + '</li>'
                    + ' <li> geo: <ol> <li>'+geo.lat+'</li>'
                    + ' <li> '+geo.lng+'</li>'
                    + ' <ol> </li>'
                    + ' </li> </td>'
                    + ' <td class="columna6">' + datos.phone + '</td>'
                    + ' <td class="columna7">' + datos.website + '</td>'
                    + ' <td class="columna8"> <ul> <li>' + company.name + '</li>'
                    + ' <li> ' + company.catchPhrase + '</li>'
                    + ' <li> ' + company.bs + '</li></tr>';
                } //else alert("Surgio un error al hacer la peticion")
            }
            http.open('GET',url,true);
            http.send();
        }
        else{
            alert('El id ingresado está fuera del rango');
        }   
    }
}
//Botones
document.getElementById("btnCargar").addEventListener("click",cargarDatos);
document.getElementById("btnLimpiar").addEventListener("click",function(){
    let res = document.getElementById('lista');
    let bus = document.getElementById('buscar');
    res.innerHTML="";
    bus.value="";
})