
const llamandoAxiosRazas = async () => {
  const url = "https://dog.ceo/api/breeds/list";

  axios
    .get(url)
    .then((res) => {
      mostrarDatosRazas(res.data);
    })
    .catch((err) => {
      console.log("Surgió un error" + err);
    });
};

const mostrarDatosRazas = (data) => {
    console.log(data);
    console.log(data.message[0]);
    const razas = document.getElementById('razas');
    for(const datos of data.message){ 
        var opcion = document.createElement("option");
        opcion.text = datos;
        razas.add(opcion);
    }
    
};

const llamandoAxiosImagen = async () => {
  const nameRaza = document.getElementById('razas').value;
    if(nameRaza == ''){
      alert('Porfavor seleccione una raza para poder ver una imagen');
    }
    else{
      const url = "https://dog.ceo/api/breed/"+nameRaza+"/images/random";

      axios
      .get(url)
      .then((res) => {
        mostrarDatosImagen(res.data);
      })
      .catch((err) => {
        console.log("Surgió un error" + err);
      });
    }
  
};

  const mostrarDatosImagen = (data) => {
    
    console.log(data.message);
    const picture = document.getElementById('imagenRaza');
    picture.innerHTML='';
    const img = document.createElement('img');
    img.src = data.message;
    picture.appendChild(img);
  };

    document.getElementById("btnCargar").addEventListener("click",llamandoAxiosRazas);
    document.getElementById("btnImagen").addEventListener("click",llamandoAxiosImagen);