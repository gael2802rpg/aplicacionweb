
  // USANDO AXIOS
  const llamandoAxios = async () => {
    const id = document.getElementById('id').value;
    if (id == ''){
        alert('Porfavor antes de buscar escriba un numero del 1 al 10')
    }
    else{
        if (id < 1 || id > 10){
            alert('El numero ingresado está fuera del rango');
        }
        else{
            const url = "https://jsonplaceholder.typicode.com/users/"+id;
            axios
                .get(url)
                .then((res) => {
                mostrarDatos(res.data);
                })
                .catch((err) => {
                console.log("Surgió un error" + err);
                });   
        }
    }
  };
  
  const mostrarDatos = (data) => {
    const name = document.getElementById("nombre");
    const username = document.getElementById("nombreU");
    const email = document.getElementById("email");
    const calle = document.getElementById("calle");
    const num_calle = document.getElementById("numeroCalle");
    const ciudad = document.getElementById("ciudad");
    
    let address = data.address;
    name.value = data.name;
    username.value = data.username;
    email.value = data.email;
    calle.value = address.street;
    num_calle.value = address.suite;
    ciudad.value = address.city;
  };
  
  function limpiar() {
    const id = document.getElementById('id');
    const name = document.getElementById("nombre");
    const username = document.getElementById("nombreU");
    const email = document.getElementById("email");
    const calle = document.getElementById("calle");
    const num_calle = document.getElementById("numeroCalle");
    const ciudad = document.getElementById("ciudad");

    id.value = '';
    name.value = '';
    username.value = '';
    email.value = '';
    calle.value = '';
    num_calle.value = '';
    ciudad.value = '';
  }
  
document.getElementById("btnBuscar").addEventListener("click", () => {
    llamandoAxios();
});
document.getElementById('btnLimpiar').addEventListener('click',()=>{
    limpiar();
});

  
