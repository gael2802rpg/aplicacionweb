const llamandoFetch = () => {

    const nombre = document.getElementById("nombre").value;
    if (nombre == ''){
      alert('Porfavor ingrese el nombre de un país antes de buscar');
    }
    else{
      const url = "https://restcountries.com/v3.1/name/"+nombre;
      
      fetch(url)
        .then((response) => response.json())
        .then((data) => mostrarDatos(data))
        .catch((error) => {
          const lblError = document.getElementById("lnlError");
          alert("Ocurrió un error: " + error);
        });
    }
  
  };

  const mostrarDatos = (data) => {
    const capital = document.getElementById('capital');
    const Lenguaje = document.getElementById('lenguaje');

    

    // Mostrando Lenguaje
    for (let leng in data[0].languages) {
        const aux = data[0].languages[leng];
        Lenguaje.innerHTML = aux;
    }
    // Mostrando Capital
    capital.innerHTML = data[0].capital;
    
  };

  function limpiar() {
    const nombre = document.getElementById("nombre");
    const capital = document.getElementById('capital');
    const Lenguaje = document.getElementById('lenguaje');

    nombre.value = '';
    capital.innerHTML='';
    Lenguaje.innerHTML='';
  }
  
  document.getElementById("btnBuscar").addEventListener("click",llamandoFetch);
  document.getElementById("btnLimpiar").addEventListener("click", limpiar);