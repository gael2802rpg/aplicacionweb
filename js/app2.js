function cargarDatos(){
    
    const http = new XMLHttpRequest;
    const bus = document.getElementById('buscar').value;
    if(bus==''){
        alert('Porfavor inserte un numero del 1 al 100 para buscar')
    }
    else{
        if(bus >= 1 && bus <= 100){
            const url = "https://jsonplaceholder.typicode.com/albums/"+bus;

            //Realizar funcion de respuesta 
            http.onreadystatechange = function(){
                
                //validar respuesta
                if(this.status == 200 && this.readyState==4){
                    
                    let res = document.getElementById('lista');
                    
                    const json = JSON.parse(this.responseText);
                    const datos = json;
                    //ciclo para mostrar los datos en la tabla
                        
                    res.innerHTML = '<tr> <td class="columna1">' + datos.userId + '</td>'
                    + ' <td class="columna2">' + datos.id + '</td>'
                    + ' <td class="columna3">' + datos.title + '</td> </tr>'


                } //else alert("Surgio un error al hacer la peticion")

            }
            http.open('GET',url,true);
            http.send();
            
        }
        else{
            alert('El id ingresado Está fuera del rango');
        } 
    }
}

//Botones
document.getElementById("btnCargar").addEventListener("click",cargarDatos);
document.getElementById("btnLimpiar").addEventListener("click",function(){

    let res = document.getElementById('lista');
    let bus = document.getElementById('buscar');
    res.innerHTML="";
    bus.value="";

})